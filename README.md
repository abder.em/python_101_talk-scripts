# Python & PANOS API HandsOn Talk - Scripts



## Getting started

To make it easy for you to follow along with the Python & PANOS API HandsOn session, I provide the beginning and end results for each session. Please do not just copy/paste these scripts but follow along with the recordings. The only way to learn scripting/coding is by doing... If you really want to add this skill, put in the time and I promise you you will get a high ROI.


## Disclaimer
You are responsible for running these scripts, we do not take any responsibility!
These scripts are not production ready, and are intended to be executed in a sandboxed lab environment as part of a learning experience for beginners.
These scripts are not complete as optimizing them is part of the phased learning experience in which we add additional knowledge layer by later.


## Project status
This repo will only be updated if there is another Python & PANOS API 101 session.
