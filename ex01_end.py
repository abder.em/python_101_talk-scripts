
# -----------------APIKEY-HERE-----------------

import requests
import xmltodict

# https://<firewall>/api/?type=keygen&user=<username>&password=<password>

sys_info_api = requests.get("https://192.168.100.185/api/?type=op&cmd=<show><system><info></info></system></show>&key=-----------------APIKEY-HERE-----------------", verify=False)

sys_info_dict = xmltodict.parse(sys_info_api.text)

sys_info_system = sys_info_dict['response']['result']['system']

hostname = sys_info_system['hostname']
ip_address = sys_info_system['hostname']
serial = sys_info_system['serial']
panos_version = sys_info_system['sw-version']

print(f"Hostname: {hostname}, IP Address: {ip_address}, Serial: {serial}, PANOS Version: {panos_version}")

# Hostname, IP Address, Serialnr, SW-version


