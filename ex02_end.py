
# -----------------APIKEY-HERE-----------------

import requests
import xmltodict
import urllib3

urllib3.disable_warnings()

######################################################## SCENARIO ########################################################

# My customer has a regular cleanup process, every quarter they manually select the policies they want to remove.
# They then tag the policies that will be cleaned up with the tag "TO_DISABLE"
# They then disable all of these policies the next quarter and add the tag "TO_DELETE"
# The quarter after that they remove the policies completely


########## Our Pseudocode

# 1 Connect to the firewall and get policies

# 2 Go through the policies
# 2.1 If Policy in unused policies list -> Add TAG TO_DISABLE
# 2.2 If Policy has TO_DISABLE tag -> Remove TO_DISABLE tag, add TO_DELETE tag, disable the policy
# 2.3 If Policy has TO_DELETE tag -> Delete the policy

# 3 Add a commit? Or to dangerous


FW_IP = "192.168.100.185"
API_ACTION_GET = "/api/?type=config&action=get"
API_PATH = "&xpath=/config/devices/entry[@name='localhost.localdomain']/vsys/entry[@name='vsys1']/rulebase/security/rules"

API_KEY = "-----------------APIKEY-HERE-----------------"


# 1 Connect to the firewall and get policies
get_policies_api = requests.get(f"https://{FW_IP}{API_ACTION_GET}{API_PATH}&key={API_KEY}", verify=False)

policies_dict = xmltodict.parse(get_policies_api.text)


policies = policies_dict['response']['result']['rules']['entry']

unused_policies = ['RULE-01', 'RULE-02', 'RULE-03']

API_ACTION_SET = "/api/?type=config&action=set"
API_ACTION_EDIT = "/api/?type=config&action=edit"
API_ACTION_DELETE = "/api/?type=config&action=delete"
DISABLE_TAG_VALUE = "TO_DISABLE"
DELETE_TAG_VALUE = "TO_DELETE"

# 2 Go through the policies

for policy in policies:


    
    if 'tag' in policy:

        # 2.3 If Policy has TO_DELETE tag -> Delete the policy
        if "TO_DELETE" in policy['tag']['member']:
            delete_policy = policy['@name']

            # Delete Policy
            requests.get(f"https://{FW_IP}{API_ACTION_DELETE}{API_PATH}/entry[@name='{delete_policy}']&key={API_KEY}", verify=False)
            print(f"Deleted policy: {delete_policy}")
            print("------------------------")

        # 2.2 If Policy has TO_DISABLE tag -> Remove TO_DISABLE tag, add TO_DELETE tag, disable the policy
        if "TO_DISABLE" in policy['tag']['member']:
            disable_policy = policy['@name']
            disable_dict = policy['tag']['member']
            # Remove TO_DISABLE tag
            
            requests.get(f"https://{FW_IP}{API_ACTION_DELETE}{API_PATH}/entry[@name='{disable_policy}']/tag&element=<tag><member>{DISABLE_TAG_VALUE}</member></tag>&key={API_KEY}", verify=False)
            
            # add TO_DELETE tag
            requests.get(f"https://{FW_IP}{API_ACTION_SET}{API_PATH}/entry[@name='{disable_policy}']&element=<tag><member>{DELETE_TAG_VALUE}</member></tag>&key={API_KEY}", verify=False)

            # disable the policy
            requests.get(f"https://{FW_IP}{API_ACTION_SET}{API_PATH}/entry[@name='{disable_policy}']&element=<disabled>yes</disabled>&key={API_KEY}", verify=False)
            print(f"Added TO_DELETE TAG and disabled policy: {disable_policy}")
            print("------------------------")

    # 2.1 If Policy in unused policies list -> Add TAG TO_DISABLE
    if policy['@name'] in unused_policies:
        unused_policy = policy['@name']

        # ADD TO_DISABLE TAG
        requests.get(f"https://{FW_IP}{API_ACTION_SET}{API_PATH}/entry[@name='{unused_policy}']&element=<tag><member>{DISABLE_TAG_VALUE}</member></tag>&key={API_KEY}", verify=False)
        print(f"Added TO_DISABLE TAG to: {unused_policy}")
        print("------------------------")
    

