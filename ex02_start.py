
# -----------------APIKEY-HERE-----------------

import requests
import xmltodict
import urllib3

urllib3.disable_warnings()

######################################################## SCENARIO ########################################################

# My customer has a regular cleanup process, every quarter they manually select the policies they want to remove.
# They then tag the policies that will be cleaned up with the tag "TO_DISABLE"
# They then disable all of these policies the next quarter and add the tag "TO_DELETE"
# The quarter after that they remove the policies completely


######### Our Pseudocode

# 1 Connect to the firewall and get policies

# 2 Go through the policies
# 2.1 If Policy in unused policies list -> Add TAG TO_DISABLE
# 2.2 If Policy has TO_DISABLE tag -> Remove TO_DISABLE tag, add TO_DELETE tag, disable the policy
# 2.3 If Policy has TO_DELETE tag -> Delete the policy

# 3 Add a commit? Or to dangerous


FW_IP = "192.168.100.185"
API_PATH = "/api/?type=op&cmd=<show><system><info></info></system></show>"
API_KEY = "-----------------APIKEY-HERE-----------------"

sys_info_api = requests.get(f"https://{FW_IP}{API_PATH}&key={API_KEY}", verify=False)

sys_info_dict = xmltodict.parse(sys_info_api.text)

print(sys_info_dict)